
ExtractData - copy data into another file
=========================================

Name
====
	extractData.py

Synopsis
========
	python extractData.py [-h] [--output OUTPUT_PATH] \
		[--list] [--source SOURCE] input_path

Description
===========
	This is a really short (and dumb) script which copies
	data from a file into another file. Just call the script
	with the input file, get the entries with list and export
	it with param --output and --source.


