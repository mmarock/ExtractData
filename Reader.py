
import re
from enum import Enum

################################################################################
##      Reader class
##
################################################################################
class Reader:
    ##
    # @brief Reader constructor
    #
    # @param data_file
    #
    # @return
    def __init__(self, data_file):
        self.file_ = open(data_file, 'r+')
        self.list_ = list()
        self.map_size_ = 512 # default value since the original data used 512
        counter = 0

        # bitch ass great regex !!!
        total_re = re.compile("\[(([A-Z]+)(\d+)(?:,(\d+)\s+)?)\]");

        # populate dictionary
        for line in self.file_:

            # totaler string
            matches = total_re.search(line)
            if matches is None:
                counter += 1
                continue

            entry = matches.group(2)
            index = int(matches.group(3))

            t = Reader.Type.StrIs(entry)
            if (entry is None) or (index is None):
                raise RuntimeError("Reader.Reader(): entry or index not found")

            if t is Reader.Type.ADC:
                self.list_.append(Reader.Entry(Reader.Type.ADC, index))
            elif t is Reader.Type.MAP:
                self.list_.append(Reader.Entry(Reader.Type.MAP, index))
            elif t is Reader.Type.SUM:
                self.list_.append(Reader.Entry(Reader.Type.SUM, index))
            elif t is Reader.Type.DIF:
                self.list_.append(Reader.Entry(Reader.Type.DIF, index))
            elif t is Reader.Type.DATA:
                e = Reader.Entry.find(self.list_, Reader.Type.ADC, index)
                # sind adc daten
                data_len = int(matches.group(4))
                if e and data_len:
                    e.setData(counter, data_len)
            elif t is Reader.Type.CDAT:
                # is either SUM, MAP or DIF
                # is it SUM?
                e = Reader.Entry.find(self.list_, Reader.Type.SUM, index)
                # is not SUM
                if e is None:
                    e = Reader.Entry.find(self.list_, Reader.Type.MAP, index)

                # is not MAP and SUM
                if e is None:
                    e = Reader.Entry.find(self.list_, Reader.Type.DIF, index)

                if e is None:
                    raise RuntimeError("{}{}: DataSet not found".format(entry, index))

                data_len = int(matches.group(4))
                if data_len is None:
                    raise RuntimeError("No data length parsed")
                e.setData(counter, data_len)

            counter+= 1
        self.file_.seek(0)

    def __str__(self):
        c = str()
        for l in self.list_:
            c += "{}\n".format(str(l))
        return c

    def __del__(self):
        if not self.file_.closed:
            self.file_.close()

    ##
    # @brief extract data lines
    #
    # @param t @see Reader.Type
    # @param index is int
    # @param output_stream something with a write() method
    #
    # @return output_stream
    def extract(self, t, index, output_stream):

        # input type check
        assert isinstance(t, Reader.Type), "t is not the right type"
        assert type(index) is int, "index must be an int"

        # get the associated entry
        e = Reader.Entry.find(self.list_, t, index)
        if e is None:
            raise RuntimeError("extract(): not found: {}{}".format(str(t), str(index)))

        # data_line is [XXXn], increment + 1 to get data
        start_line = e.data_line_ + 1
        last_line = e.data_line_ + e.data_len_
        if start_line < 0 or e.data_len_ < 0:
            raise RuntimeError("extract(): no data: line:{}, len:{}".format( \
                    e.data_line_, e.data_len_))

        # TODO: Is there a better file position marker than counting lines?
        line_counter = 0
        reformat_counter = 1
        for l in self.file_:
            if line_counter < start_line:
                line_counter+= 1
                continue
            # start line + 1
            if line_counter > last_line:
                break

            if t is Reader.Type.MAP:
                if reformat_counter % self.map_size_ != 0:
                    output_stream.write(l.strip("\n") + " ")
                else:
                    output_stream.write(l)
                reformat_counter += 1
            else:
                output_stream.write(l)

            line_counter+= 1

        # reset the file to beginning
        self.file_.seek(0)
        return output_stream

    def haveEntry(self, t, index):

        assert isinstance(t, Reader.Type), "t is not the right type"
        assert type(index) is int, "index must be an int"
        e = Reader.Entry.find(self.list_, t, index)
        if (e is None) or (e.data_line_ <= 0) or (e.data_len_ <= 0):
            return False
        else:
            return True

    def getEntries(self):
        return self.list_

################################################################################
##      Reader.Type class
################################################################################
    class Type(Enum):
        ADC = 1
        MAP = 2
        SUM = 3
        DIF = 4
        DATA = 5
        CDAT = 6
        def __str__(self):
            return {
                    Reader.Type.ADC: "ADC",
                    Reader.Type.MAP: "MAP",
                    Reader.Type.SUM: "SUM",
                    Reader.Type.DIF: "DIF",
                    Reader.Type.DATA: "DATA",
                    Reader.Type.CDAT: "CDAT",
                    }[self]

        @staticmethod
        def StrIs(string):
            if string == "ADC":
                return Reader.Type['ADC']
            if string == "MAP":
                return Reader.Type['MAP']
            if string == "SUM":
                return Reader.Type['SUM']
            if string == "DIF":
                return Reader.Type['DIF']
            if string == "CDAT":
                return Reader.Type['CDAT']
            if string == "DATA":
                return Reader.Type['DATA']
            return None


################################################################################
##      Reader.Entry class
################################################################################
    class Entry:
        def __init__(self, t, index):
            assert isinstance(t,Reader.Type), "t is the wrong type"
            assert type(index) is int, "index must be an int"
            self.type_ = t
            self.index_ = index
            self.data_line_ = -1
            self.data_len_ = -1

        def __str__(self):
            return "{}{}\tline:{:8}\tlen:{:8}".format(
                    str(self.type_),
                    str(self.index_),
                    self.data_line_,
                    self.data_len_)

        def haveData(self):
            return self.data_line_ >= 0

        def setData(self, line_no, data_len):
            self.data_line_ = line_no
            self.data_len_ = data_len

        @staticmethod
        def find(entry_list, t, index):
            for e in entry_list:
                if (e.type_ is t) and (e.index_ is index):
                    return e
            return None
