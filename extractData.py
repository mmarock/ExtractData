

#!/usr/bin/python

import argparse, os, sys
from Reader import Reader
import re

def main():
    parser = argparse.ArgumentParser(description = "Cut out ascii data from single ADC/MAP/SUM sections")

    parser.add_argument('input_path', metavar='input_path', type=str, help='set the input_file')
    parser.add_argument('--output', dest='output_path', help='set an output file')
    parser.add_argument('--list', dest='list', help='print all available sources', action='store_true')
    parser.add_argument('--map-size', dest='map_size', help='specify the map-size, defaults to 512', type=int)
    parser.add_argument('--source', dest='source', help='specify the source (ADCx | SUMx | MAPx | DIFx)', type=str)

    args = parser.parse_args()

    reader = Reader(args.input_path)

    if args.list:
        # list the contained data
        print(reader)
        sys.exit(0)

    if not args.source:
        # no source given
        print("{}: specify a source like --source ADCn".format(__file__))
        sys.exit(0)

    if not args.output_path:
        # no output path -> to stdout
        print("{}: no --output given".format(__file__))
        sys.exit(0)

    if os.path.exists(args.output_path):
        # path already exists
        print("{}: {} already exists".format(__file__, args.output_path))
        sys.exit(0)

    match = re.compile("(([A-Z]+)(\d+))").search(args.source)
    if not match:
        print("{}: Write something like \"ADC0\"".format(__file__))
        sys.exit(0)

    t = Reader.Type.StrIs(match.group(2))
    if t is Reader.Type.MAP:
        if args.map_size:
            reader.map_size_ = args.map_size_
        else:
            print ("{}: type is MAP, consider use --map-size (default is {})".format(__file__, reader.map_size_))

    assert isinstance(t, Reader.Type), "did not return a valid type"
    if reader.haveEntry(t, int(match.group(3))) is False:
        print ("{}: Entry {} not found".format(__file__, match.group(1)))
        sys.exit(0)

    print("{}: Try writing Entry {} -> {}".format(__file__, match.group(1), args.output_path))
    out = open(args.output_path, 'w')
    try:
        reader.extract(t, int(match.group(3)), out)
        print("{}: finished".format(__file__))
        out.close()
    except Exception as err:
        print("{}: {}".format(__file__,err))
        out.close()
        os.remove(args.output_path)

if __name__ == "__main__":
    main()
